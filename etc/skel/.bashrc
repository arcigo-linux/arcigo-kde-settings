#
# Custom ~/.bashrc config for Arcigo linux !
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Add to path
if [ -d "$HOME/.bin" ] ;
  then PATH="$HOME/.bin:$PATH"
fi

if [ -d "$HOME/.local/bin" ] ;
  then PATH="$HOME/.local/bin:$PATH"
fi

# Bash prompt PS1
if [[ "`id -u`" -eq 0 ]]; then
	export PS1="\[\033[38;5;1m\][\[$(tput sgr0)\]\[\033[38;5;9m\]\u@\h\[$(tput sgr0)\] \[$(tput sgr0)\]\[\033[38;5;4m\]\w\[$(tput sgr0)\]\[\033[38;5;1m\]]\[$(tput sgr0)\]\\$ \[$(tput sgr0)\]"
else
	export PS1="\[\033[38;5;21m\][\[$(tput sgr0)\]\[\033[38;5;33m\]\u@\h\[$(tput sgr0)\] \w\[$(tput sgr0)\]\[\033[38;5;21m\]]\[$(tput sgr0)\]\\$ \[$(tput sgr0)\]"
fi

# Export Defaults
export EDITOR='nano'
export VISUAL='nano'
export HISTCONTROL=ignoreboth:erasedups
export PAGER='less'

## Use the up and down arrow keys for finding a command in history
## (you can write some initial letters of the command first).
bind '"\e[A":history-search-backward'
bind '"\e[B":history-search-forward'
#ignore upper and lowercase when TAB completion
bind "set completion-ignore-case on"

# Listing files
alias ls='ls --color=auto'
alias l='ls -lav --ignore=.?*'   # show long listing but no hidden dotfiles except "."
alias la='ls -a'
alias ll='ls -alFh'

## Colorize the grep command output for ease of use (good for log files)##
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# Git aliases
alias gstat='git status'
alias gadd='git add .'
alias gpush='git push'
alias gcom='git commit -m "Updated"'
alias rmgitcache="rm -r ~/.cache/git"

# Error messages from journalctl
alias jctl="journalctl -p 3 -xb"

# Neovim
alias vim='nvim'

# pacman aliases
alias install='sudo pacman -S'
alias update='sudo pacman -Syyu'
alias update-force='sudo pacman -Syy'
alias update-aur='yay -Syu'

# Clean orphaned packages
alias cleanup='sudo pacman -Rns $(pacman -Qtdq)'

# Remove pacman db lock
alias unlock='sudo rm /var/lib/pacman/db.lck'

# Add new fonts
alias update-fc='sudo fc-cache -fv'

# Aliases to edit important configuration files
alias ed-lightdm="sudo $EDITOR /etc/lightdm/lightdm.conf"
alias ed-pacman="sudo $EDITOR /etc/pacman.conf"
alias ed-grub="sudo $EDITOR /etc/default/grub"
alias ed-confgrub="sudo $EDITOR /boot/grub/grub.cfg"
alias ed-mkinitcpio="sudo $EDITOR /etc/mkinitcpio.conf"
alias ed-mirrorlist="sudo $EDITOR /etc/pacman.d/mirrorlist"
alias ed-arcigo-mirrorlist='sudo nano /etc/pacman.d/arcigo-mirrorlist'
alias ed-sddm="sudo $EDITOR /etc/sddm.conf"
alias ed-sddmk="sudo $EDITOR /etc/sddm.conf.d/kde_settings.conf"
alias ed-fstab="sudo $EDITOR /etc/fstab"
alias ed-nsswitch="sudo $EDITOR /etc/nsswitch.conf"
alias ed-samba="sudo $EDITOR /etc/samba/smb.conf"
alias ed-gnupgconf="sudo nano /etc/pacman.d/gnupg/gpg.conf"
alias ed-hosts="sudo $EDITOR /etc/hosts"
alias ed-bashrc="$EDITOR ~/.bashrc"

# Hardware info
alias hw="hwinfo --short"

# youtube-dl
alias yt-mp3="youtube-dl --extract-audio --audio-format mp3"
alias yt-480='youtube-dl -f "bestvideo[height<=480][ext=mp4]+bestaudio[ext=m4a]" '
alias yt-720='youtube-dl -f "bestvideo[height<=720][ext=mp4]+bestaudio[ext=m4a]" '
alias yt-best='youtube-dl -f "bestvideo[ext=mp4]+bestaudio[ext=m4a]/bestvideo+bestaudio" '

# Copy shell config
alias cp-bash-config='cp /etc/skel/.bashrc ~/.bashrc && source ~/.bashrc'

# Check for free space
alias free='free -mt'

# Continue the download
alias wget='wget -c'

# To create a instant web server
alias serv-now='python3 -m http.server'

# readable output of df
alias df='df -h'

# Update grub configuration
alias update-grub="grub-mkconfig -o /boot/grub/grub.cfg"

#systeminfo
alias probe="sudo -E hw-probe -all -upload"
alias sysfailed="systemctl list-units --failed"

# Shopt
shopt -s autocd # change to named directory
shopt -s cdspell # autocorrects cd misspellings
shopt -s cmdhist # save multi-line commands in history as single line
shopt -s dotglob
shopt -s histappend # do not overwrite history
shopt -s expand_aliases # expand aliases

# Report system info
# neofetch

# pullout is a extractor for all kind of archives
# usage: pullout <file>
pullout ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)  tar xjf $1
                  echo -e "\n[+] '$1' extracted successfully. !\n"   ;;
      *.tar.gz)   tar xzf $1
            		  echo -e "\n[+] '$1' extracted successfully. !\n"   ;;
      *.bz2)      bunzip2 $1
            		  echo -e "\n[+] '$1' extracted successfully. !\n"   ;;
      *.rar)      unrar x $1
            		  echo -e "\n[+] '$1' extracted successfully. !\n"   ;;
      *.gz)       gunzip $1
            		  echo -e "\n[+] '$1' extracted successfully. !\n"   ;;
      *.tar)      tar xf $1
            		  echo -e "\n[+] '$1' extracted successfully. !\n"   ;;
      *.tbz2)     tar xjf $1
            		  echo -e "\n[+] '$1' extracted successfully. !\n"   ;;
      *.tgz)      tar xzf $1
            		  echo -e "\n[+] '$1' extracted successfully. !\n"   ;;
      *.zip)      unzip $1
            		  echo -e "\n[+] '$1' extracted successfully. !\n"   ;;
      *.Z)        uncompress $1
            		  echo -e "\n[+] '$1' extracted successfully. !\n"   ;;
      *.7z)       7z x $1
            		  echo -e "\n[+] '$1' extracted successfully. !\n"   ;;
      *.deb)      ar x $1
            		  echo -e "\n[+] '$1' extracted successfully. !\n"   ;;
      *.tar.xz)   tar xf $1
            		  echo -e "\n[+] '$1' extracted successfully. !\n"   ;;
      *.tar.zst)  tar xf $1
                  echo -e "\n[+] '$1' extracted successfully. !\n"   ;;

      *)          echo -e "\n[-] Usage: pullout <ArchiveFile>\n"      ;;
    esac
  else
    echo "'$1' is not a valid archive file to extract."
  fi
}

# For instant file sharing
# File sharing via 0x0.st
function share ()
{
  local server actual_file_size max_file_size
  max_file_size="512000" # 512000
  server="https://0x0.st"

  if [[ "$@" = "" ]]; then
    echo -e "\n[-] Usage: share <File>\n"
  else
    if [[ ! -f $1 ]]; then
      echo -e "\n[+] '$1' is not a valid file.\n"
    else
      actual_file_size=$(wc -c $1 | cut -d' ' -f1)
      if [[ ! $actual_file_size -le $max_file_size ]]; then
        echo -e "\n[-] File size is greater than ${max_file_size}.\n"
	      notify-send "File cannot be Uploaded !" "File size is greater than ${max_file_size}"
      else
        echo -e "\nFILE = ($1)\n"
        read -p "-> Want to send the file ? If yes [ENTER] to continue... " junk
        link=$(curl --silent -F"file=@${1}" ${server} | tee /tmp/123.txt)
        echo -e "\n-> File '${1}' sent successfully!"
        echo -e "\n-> Here is the link: ${link}\n"
      fi
    fi
  fi
}
