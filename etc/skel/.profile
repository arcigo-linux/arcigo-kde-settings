export BROWSER=brave
export TERM=konsole
export MAIL=thunderbird
export GTK3_RC_FILES="$HOME/.gtkrc-3.0"
export EDITOR='nano'
export VISUAL='nano'
export HISTCONTROL=ignoreboth:erasedups
export PAGER='less'
